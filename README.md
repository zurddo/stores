###### Store Services 

TL;DR

**INSTALLATION** 
To run the application you can run the following commands. You need to have docker and docker compose installed.

`mvn package`
`docker-compose up --build`  

Otherwise, you need to change the connection configuration to the DB manually in the application.properties
to your database, then generate the war with `mvn package` and copy the generated war in the target folder to 
your _tomcat/webapps/_ directory 

For the purpose of this example the application will be deployed as stores.war hence the WSDL will be at http://localhost:8080/stores/ws/stores.wsdl
that could be changed and be deployed into root changing the name of the war file to ROOT.war

Implement three SOAP endpoints and deploy them into a Tomcat container and use a postgresql database.

Database structure: 

- merchant

| id | create_date | name | lastname | birthdate | update_date |
| --- | ----------- | ---- | -------- | --------- | --- |
| int | timestamp | varchar | varchar | date | timestamp |

- product

| id | create_date | label | unit_price | currency | weight | height | update_date |
| --- | --- | --- | --- | --- | --- | --- | --- |
|  int | timestamp | varchar | numeric | varchar | numeric | numeric | timestamp | 

- address  

| id | number | street | zipcode | update_date |
| --- | --- | --- | --- | --- | 
| int | int | varchar | varchar | timestamp | 

Some rules:

✓A merchant could have a relationship 0..n to products

✓A product could have a relationship 0..n merchants

✓We want to persist the update date between products and merchants
    - Add update_date to schemas
    
✓A merchant could have a relationship 0..n addresses

_For this example I'm using a docker image to have postgres_ 

Using postgres image: https://hub.docker.com/_/postgres

**Endpoints**

Create three endpoints to do the following operations:

✓ Add/update/delete merchant

✓ Add/update/delete product

✓ Associate a merchant to a product

---

Using Spring boot starters, it is possible to use Spring Boot and modify the application to generate a war file 
and start the application in order to deploy externally. 

Added lombok library to skip getter, setters, constructors and have builder.

To use postgres with docker run the following command:

`docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_USER=myuser -e POSTGRES_DB=store -d postgres`

Change in the application.properties to your database, user and password configuration.

No need to SQL scripts since Spring JPA + Hibernate takes care of creating the SQL schema even with relationships.

For the purpose of this example I'm using a "traditional" structure: model, repositories, services, controller, etc.

WSDL under (running in local): http://localhost:8080/ws/stores.wsdl
WSDL under Docker + Tomcat: http://localhost:8080/stores/ws/stores.wsdl

Since association of a merchant with a product is in a different service, I'm assuming would be the same to associate an address with merchant and not at creation. 

TODO: Exception Handler, Retrieve Merchants, Products, Associate Address to Merchants (Not required).