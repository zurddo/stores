package com.example.store.services;

import com.example.store.entities.Product;
import com.example.store.mappers.ProductMapper;
import com.example.store.model.AddProductRequest;
import com.example.store.repositories.ProductRepository;
import org.hibernate.exception.JDBCConnectionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ProductServiceTests {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    private IProductService productService;

    @BeforeEach
    private void init() {
        productService = new ProductServiceImpl(productRepository, productMapper);
    }

    @Test
    public void whenAddProduct_thenReturnNewProductTest() {

        Product productEntity = getProductEntity();
        com.example.store.model.Product productModel = new com.example.store.model.Product();
        productModel.setProductId(1);
        productModel.setLabel("First");
        productModel.setCurrency("EUR");
        productModel.setHeight(8d);
        productModel.setWeight(1d);
        productModel.setUnitPrice(4.5d);

        AddProductRequest addProductRequest = mock(AddProductRequest.class);
        when(productMapper.productToProductEntity(addProductRequest)).thenReturn(productEntity);
        when(productRepository.save(productEntity)).thenReturn(productEntity);
        when(productMapper.productEntityToProduct(productEntity)).thenReturn(productModel);
        com.example.store.model.Product productResult = productService.addProduct(addProductRequest);

        assertNotNull(productResult);
        assertEquals(1, productResult.getProductId());
        assertEquals("First", productResult.getLabel());
        assertEquals("EUR", productResult.getCurrency());
        assertEquals(8d, productResult.getHeight());
        assertEquals(1d, productModel.getWeight());
        assertEquals(4.5d, productResult.getUnitPrice());

    }

    @Test
    public void whenUpdateProduct_thenTrueTest() {

        com.example.store.model.Product productToUpdate = new com.example.store.model.Product();
        productToUpdate.setProductId(1L);
        productToUpdate.setLabel("First");
        productToUpdate.setCurrency("EUR");
        productToUpdate.setHeight(8d);
        productToUpdate.setWeight(1d);
        productToUpdate.setUnitPrice(4.5d);

        Product productEntity = getProductEntity();

        Product productUpdated = new Product();
        productUpdated.setId(1L);
        productUpdated.setLabel("First");
        productUpdated.setCurrency("EUR");
        productUpdated.setWeight(1d);
        productUpdated.setUnitPrice(BigDecimal.valueOf(4.5));

        when(productRepository.findById(productToUpdate.getProductId())).thenReturn(java.util.Optional.of(productEntity));
        when(productRepository.save(productEntity)).thenReturn(productUpdated);

        boolean isUpdated = productService.updateProduct(productToUpdate);

        assertTrue(isUpdated);

    }

    @Test
    public void whenUpdateProductNotInDb_thenFalseTest() {

        com.example.store.model.Product productToUpdate = new com.example.store.model.Product();
        when(productRepository.findById(productToUpdate.getProductId())).thenReturn(null);

        boolean isUpdated = productService.updateProduct(productToUpdate);

        assertFalse(isUpdated);
    }

    @Test
    public void whenDeleteProduct_thenTrueTest(){

        doNothing().when(productRepository).deleteById(anyLong());

        boolean isDeleted = productService.deleteProductById(1);

        assertTrue(isDeleted);

    }

    @Test
    public void whenDeleteProduct_thenThrowExceptionTest(){

        doThrow(JDBCConnectionException.class).when(productRepository).deleteById(anyLong());

        boolean isDeleted = productService.deleteProductById(1);

        assertFalse(isDeleted);
    }

    private Product getProductEntity() {
        Product productEntity = new Product();
        productEntity.setId(1L);
        productEntity.setLabel("First");
        productEntity.setCurrency("EUR");
        productEntity.setHeight(5d);
        productEntity.setWeight(5d);
        return productEntity;
    }
}
