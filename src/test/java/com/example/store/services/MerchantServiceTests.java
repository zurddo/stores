package com.example.store.services;

import com.example.store.entities.Merchant;
import com.example.store.entities.Product;
import com.example.store.mappers.MerchantMapper;
import com.example.store.model.AddMerchantRequest;
import com.example.store.repositories.MerchantRepository;
import com.example.store.repositories.ProductRepository;
import org.hibernate.exception.JDBCConnectionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@SpringBootTest
public class MerchantServiceTests {

    @Mock
    private MerchantRepository merchantRepository;

    @Mock
    private MerchantMapper merchantMapper;

    @Mock
    private ProductRepository productRepository;

    private IMerchantService merchantService;

    @BeforeEach
    private void init() {
        merchantService = new MerchantServiceImpl(merchantRepository, merchantMapper, productRepository);
    }

    @Test
    public void whenAddNewMerchant_thenNewMerchantTest() {

        AddMerchantRequest addMerchantRequest = mock(AddMerchantRequest.class);

        com.example.store.model.Merchant merchantModel = new com.example.store.model.Merchant();
        merchantModel.setName("Test");
        merchantModel.setLastName("LastName");
        merchantModel.setMerchantId(1L);

        Merchant merchantEntity = getMerchantEntity();
        when(merchantMapper.merchantToMerchantEntity(addMerchantRequest)).thenReturn(merchantEntity);
        when(merchantRepository.save(merchantEntity)).thenReturn(merchantEntity);
        when(merchantMapper.merchantEntityToMerchant(merchantEntity)).thenReturn(merchantModel);

        com.example.store.model.Merchant merchant = merchantService.addMerchant(addMerchantRequest);

        assertNotNull(merchant);
        assertEquals("Test", merchant.getName());
        assertEquals("LastName", merchant.getLastName());
        assertEquals(1L, merchant.getMerchantId());
    }

    @Test
    public void whenUpdateMerchant_thenTrueTest() throws Exception {
        com.example.store.model.Merchant merchantToUpdate = new com.example.store.model.Merchant();
        merchantToUpdate.setMerchantId(1L);
        merchantToUpdate.setName("Test1");
        merchantToUpdate.setLastName("Test2");
        merchantToUpdate.setBirthDate(getGregorianCalendar());

        Merchant merchantEntity = getMerchantEntity();

        Merchant merchantUpdated = new Merchant();
        merchantUpdated.setId(1L);
        merchantUpdated.setName("Updated");
        merchantUpdated.setLastName("Updated2");

        when(merchantRepository.findById(merchantToUpdate.getMerchantId())).thenReturn(java.util.Optional.of(merchantEntity));
        when(merchantRepository.save(merchantEntity)).thenReturn(merchantUpdated);

        boolean isUpdated = merchantService.updateMerchant(merchantToUpdate);

        assertTrue(isUpdated);

    }

    @Test
    public void whenUpdateMerchantNotInDb_thenFalseTest() {

        com.example.store.model.Merchant merchantToUpdate = new com.example.store.model.Merchant();
        when(productRepository.findById(any())).thenReturn(null);

        boolean isUpdated = merchantService.updateMerchant(merchantToUpdate);

        assertFalse(isUpdated);
    }

    @Test
    public void whenDeleteMerchant_thenTrueTest(){

        doNothing().when(merchantRepository).deleteById(anyLong());

        boolean isDeleted = merchantService.deleteMerchantById(1);

        assertTrue(isDeleted);

    }

    @Test
    public void whenDeleteMerchantAndThrowException_thenFalseTest(){

        doThrow(JDBCConnectionException.class).when(merchantRepository).deleteById(anyLong());

        boolean isDeleted = merchantService.deleteMerchantById(1);

        assertFalse(isDeleted);
    }

    @Test
    public void whenAddProductToMerchant_thenMerchantUpdated() throws Exception{

        Product productEntity = new Product();
        productEntity.setId(1L);
        productEntity.setLabel("First");
        productEntity.setCurrency("EUR");
        productEntity.setHeight(5d);
        productEntity.setWeight(5d);

        Merchant merchant = getMerchantEntity();

        when(productRepository.findById(anyLong())).thenReturn(java.util.Optional.of(productEntity));
        when(merchantRepository.findById(anyLong())).thenReturn(java.util.Optional.of(merchant));

        when(merchantRepository.save(any())).thenReturn(merchant);
        when(merchantMapper.merchantEntityToMerchant(any())).thenReturn(getMerchantModel());

        com.example.store.model.Merchant merchantWithProduct = merchantService.addProductToMerchant(1L, 1L);

        assertNotNull(merchantWithProduct);
        assertEquals("Test", merchantWithProduct.getName());
        assertEquals("Test2", merchantWithProduct.getLastName());
        assertEquals(getGregorianCalendar(), merchantWithProduct.getBirthDate());
        // check how to validate list since Merchant model doesn't have setter for product list nor addresses.

    }

    private Merchant getMerchantEntity() {
        Merchant merchant = new Merchant();
        merchant.setId(1L);
        merchant.setName("Test");
        merchant.setLastName("LastName");
        merchant.setBirthDate(LocalDate.of(2000, 4, 2));
        LocalDateTime localDateTime = LocalDateTime.of(2020, 2, 3,4,5);
        merchant.setCreatedAt(localDateTime.toInstant(ZoneOffset.UTC));
        return merchant;
    }

    private com.example.store.model.Merchant getMerchantModel() throws Exception{
        com.example.store.model.Merchant merchantModel = new com.example.store.model.Merchant();
        merchantModel.setMerchantId(1L);
        merchantModel.setName("Test");
        merchantModel.setLastName("Test2");
        merchantModel.setBirthDate(getGregorianCalendar());
        return merchantModel;
    }

    private XMLGregorianCalendar getGregorianCalendar() throws DatatypeConfigurationException {
        LocalDate localDate = LocalDate.of(2019, 4, 25);

        return DatatypeFactory.newInstance().newXMLGregorianCalendar(localDate.toString());
    }
}
