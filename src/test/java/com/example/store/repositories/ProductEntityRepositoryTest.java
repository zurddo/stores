package com.example.store.repositories;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test just the injection of repository in a auto configured in-memory database since we don't have custom queries
 * If special query added then it should be tested here.
 */
@DataJpaTest
public class ProductEntityRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    void injectedComponentsAreNotNull(){
        assertThat(productRepository).isNotNull();
    }
}
