package com.example.store.mappers;

import com.example.store.entities.MerchantProduct;
import com.example.store.entities.Product;
import com.example.store.model.AddProductRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product productToProductEntity(AddProductRequest product);

    @Mapping(source = "id", target = "productId")
    com.example.store.model.Product productEntityToProduct(Product product);

    List<com.example.store.model.Product> productsEntityToProducts(List<Product> productList);

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "product.label", target = "label")
    @Mapping(source = "product.unitPrice", target = "unitPrice")
    @Mapping(source = "product.currency", target = "currency")
    @Mapping(source = "product.weight", target = "weight")
    @Mapping(source = "product.height", target = "height")
    com.example.store.model.Product merchantProductToProduct(MerchantProduct merchantProduct);
}
