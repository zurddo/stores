package com.example.store.mappers;

import com.example.store.entities.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    Address addressDTOToAddress(final com.example.store.model.Address address);

    com.example.store.model.Address addressToAddressDTO(final Address address);

}
