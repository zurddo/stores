package com.example.store.mappers;

import com.example.store.entities.Merchant;
import com.example.store.model.AddMerchantRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = ProductMapper.class)
public interface MerchantMapper {

    Merchant merchantToMerchantEntity(AddMerchantRequest merchant);

    @Mapping(source = "id", target = "merchantId")
    com.example.store.model.Merchant merchantEntityToMerchant(Merchant merchant);

    List<com.example.store.model.Merchant> merchantsEntityToMerchant(List<Merchant> merchantList);
}
