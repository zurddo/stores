package com.example.store.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "merchant")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Merchant extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String lastName;

    private LocalDate birthDate;

    @OneToMany(mappedBy = "merchant", cascade = CascadeType.MERGE, orphanRemoval = true)
    private List<MerchantProduct> productList = new ArrayList<>();

    @OneToMany(mappedBy = "merchant")
    private List<Address> addresses;

    public void addProduct(Product product){
        MerchantProduct merchantProduct = new MerchantProduct(this, product);
        productList.add(merchantProduct);
        product.getMerchantList().add(merchantProduct);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Merchant merchant = (Merchant) o;

        if (getId() != null ? !getId().equals(merchant.getId()) : merchant.getId() != null) return false;
        if (getName() != null ? !getName().equals(merchant.getName()) : merchant.getName() != null) return false;
        if (getLastName() != null ? !getLastName().equals(merchant.getLastName()) : merchant.getLastName() != null)
            return false;
        if (getBirthDate() != null ? !getBirthDate().equals(merchant.getBirthDate()) : merchant.getBirthDate() != null)
            return false;
        return getAddresses() != null ? getAddresses().equals(merchant.getAddresses()) : merchant.getAddresses() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getBirthDate() != null ? getBirthDate().hashCode() : 0);
        return result;
    }
}
