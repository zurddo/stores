package com.example.store.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

/**
 * Join table entity for the many-to-many relationship
 */
@Entity
@Table(name = "merchant_product")
@Getter
@Setter
@EqualsAndHashCode
public class MerchantProduct extends AuditModel{

    @EmbeddedId
    private MerchantProductId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("merchantId")
    private Merchant merchant;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("productId")
    private Product product;

    public MerchantProduct() {
    }

    public MerchantProduct(Merchant merchant, Product product){
        this.merchant = merchant;
        this.product = product;
        this.id = new MerchantProductId(merchant.getId(), product.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MerchantProduct that = (MerchantProduct) o;

       return Objects.equals(merchant, that.merchant) &&
               Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        int result = getMerchant() != null ? getMerchant().hashCode() : 0;
        result = 31 * result + (getProduct() != null ? getProduct().hashCode() : 0);
        return result;
    }
}
