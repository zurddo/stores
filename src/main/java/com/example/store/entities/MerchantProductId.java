package com.example.store.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Class that defines the composite key for the join table
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class MerchantProductId implements Serializable {

    @Column(name = "merchant_id")
    private Long merchantId;

    @Column(name = "product_id")
    private Long productId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MerchantProductId that = (MerchantProductId) o;

        if (getMerchantId() != null ? !getMerchantId().equals(that.getMerchantId()) : that.getMerchantId() != null)
            return false;
        return getProductId() != null ? getProductId().equals(that.getProductId()) : that.getProductId() == null;
    }

    @Override
    public int hashCode() {
        int result = getMerchantId() != null ? getMerchantId().hashCode() : 0;
        result = 31 * result + (getProductId() != null ? getProductId().hashCode() : 0);
        return result;
    }
}
