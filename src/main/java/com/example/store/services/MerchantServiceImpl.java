package com.example.store.services;

import com.example.store.entities.Product;
import com.example.store.mappers.MerchantMapper;
import com.example.store.model.AddMerchantRequest;
import com.example.store.model.Merchant;
import com.example.store.repositories.MerchantRepository;
import com.example.store.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class MerchantServiceImpl implements IMerchantService{

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());
    private final MerchantRepository merchantRepository;
    private final MerchantMapper merchantMapper;
    private final ProductRepository productRepository;

    public MerchantServiceImpl(MerchantRepository merchantRepository, MerchantMapper merchantMapper, ProductRepository productRepository) {
        this.merchantRepository = merchantRepository;
        this.merchantMapper = merchantMapper;
        this.productRepository = productRepository;
    }

    @Override
    public Merchant getMerchantById(long merchantId) {
        com.example.store.entities.Merchant merchant = merchantRepository.findById(merchantId).orElse(null);
        return merchantMapper.merchantEntityToMerchant(merchant);
    }

    @Override
    public List<Merchant> getAllMerchants() {
        List<com.example.store.entities.Merchant> merchantList = merchantRepository.findAll();
        return merchantMapper.merchantsEntityToMerchant(merchantList);
    }

    @Override
    public Merchant addMerchant(AddMerchantRequest merchant) {
        try {
            com.example.store.entities.Merchant merchantEntity = merchantMapper.merchantToMerchantEntity(merchant);
            merchantEntity = merchantRepository.save(merchantEntity);
            return merchantMapper.merchantEntityToMerchant(merchantEntity);
        }catch (Exception e){
            LOG.error(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean updateMerchant(Merchant merchant) {
        try {
            com.example.store.entities.Merchant merchantToUpdate =
                    merchantRepository.findById(merchant.getMerchantId()).orElse(null);
            if(merchantToUpdate == null){
                return false;
            } else {
                merchantToUpdate.setBirthDate(convertBirthDay(merchant.getBirthDate()));
                merchantToUpdate.setLastName(merchant.getLastName());
                merchantToUpdate.setName(merchant.getName());
                merchantRepository.save(merchantToUpdate);
                return true;
            }
        }catch (Exception e){
            LOG.error(e.getMessage());
            return false;
        }

    }

    @Override
    public boolean deleteMerchantById(long merchantId) {
        try {
            merchantRepository.deleteById(merchantId);
            return true;
        } catch (Exception e){
            LOG.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Merchant addProductToMerchant(long merchantId, long productId) {
        try{
            // find merchant and product
            Product product = productRepository.findById(productId).orElse(null);
            com.example.store.entities.Merchant merchant = merchantRepository.findById(merchantId).orElse(null);

            // Both must exist to be associated. Not handle exception for now, it's a to do
            if(product == null || merchant == null) {
                return null;
            }

            merchant.addProduct(product);

            merchant = merchantRepository.save(merchant);

            return merchantMapper.merchantEntityToMerchant(merchant);

        }catch (Exception e){
            LOG.error(e.getMessage());
            return null;
        }
    }

    private LocalDate convertBirthDay(XMLGregorianCalendar birthDate) {
        return LocalDate.of(birthDate.getYear(), birthDate.getMonth(), birthDate.getDay());
    }
}
