package com.example.store.services;

import com.example.store.model.AddProductRequest;
import com.example.store.model.Product;

import java.util.List;

public interface IProductService {

    public Product getEntityById(long productId);
    public List<Product> getAllEntities();
    public Product addProduct(AddProductRequest product);
    public boolean updateProduct(Product product);
    public boolean deleteProductById(long productId);

}
