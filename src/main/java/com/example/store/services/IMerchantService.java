package com.example.store.services;

import com.example.store.model.AddMerchantRequest;
import com.example.store.model.Merchant;

import java.util.List;

public interface IMerchantService {

    public Merchant getMerchantById(long merchantId);
    public List<Merchant> getAllMerchants();
    public Merchant addMerchant(AddMerchantRequest merchant);
    public boolean updateMerchant(Merchant merchant);
    public boolean deleteMerchantById(long merchantId);

    Merchant addProductToMerchant(long merchantId, long productId);
}
