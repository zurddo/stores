package com.example.store.services;

import com.example.store.mappers.AddressMapper;
import com.example.store.model.Address;
import com.example.store.repositories.AddressRepository;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public AddressService(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    public Address getAddressById(Long id) {
        Address address = new Address();
        address.setStreet("Me voy");
        address.setZipcode("44000");
        return address;
        //return addressMapper.addressToAddressDTO(addressRepository.findById(id).orElse(null));
    }

}
