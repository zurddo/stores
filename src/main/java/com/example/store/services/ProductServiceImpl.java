package com.example.store.services;

import com.example.store.mappers.ProductMapper;
import com.example.store.model.AddProductRequest;
import com.example.store.model.Product;
import com.example.store.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class ProductServiceImpl implements IProductService {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public Product getEntityById(long productId) {
        com.example.store.entities.Product product = productRepository.findById(productId).orElse(null);
        return productMapper.productEntityToProduct(product);
    }

    @Override
    public List<Product> getAllEntities() {
        List<com.example.store.entities.Product> productList = productRepository.findAll();
        return productMapper.productsEntityToProducts(productList);
    }

    @Override
    public Product addProduct(AddProductRequest product) {
        try {
            com.example.store.entities.Product productEntity = productMapper.productToProductEntity(product);
            productEntity = productRepository.save(productEntity);
            return productMapper.productEntityToProduct(productEntity);
        }catch (Exception e) {
            LOG.error(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean updateProduct(Product product) {
        // Find the product to update
        try {
            com.example.store.entities.Product productToUpdate = productRepository.findById(product.getProductId()).orElse(null);
            if(productToUpdate == null){
                return false; // Should return a better message that product doesn't exists
            } else { // update
                productToUpdate.setCurrency(product.getCurrency());
                productToUpdate.setHeight(product.getHeight());
                productToUpdate.setLabel(product.getLabel());
                productToUpdate.setUnitPrice(BigDecimal.valueOf(product.getUnitPrice()));
                productToUpdate.setWeight(product.getWeight());
                productRepository.save(productToUpdate);
                return true;
            }
        } catch (Exception e){
            LOG.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteProductById(long productId) {
        try {
            productRepository.deleteById(productId);
            return true;
        } catch (Exception e){
            LOG.error(e.getMessage());
            return false;
        }
    }
}
