package com.example.store.web;

import com.example.store.model.*;
import com.example.store.services.IMerchantService;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class MerchantEndpoint {

    private static final String NAMESPACE_URI = "http://mytest.com/gs-producing-web-service";

    private final IMerchantService merchantService;

    public MerchantEndpoint(IMerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addMerchantRequest")
    @ResponsePayload
    public AddMerchantResponse addMerchant(@RequestPayload AddMerchantRequest request){
        final AddMerchantResponse response = new AddMerchantResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();
        Merchant merchant = merchantService.addMerchant(request);

        if(merchant == null){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding a Merchant");
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Merchant Added Successfully");
        }

        response.setMerchant(merchant);
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateMerchantRequest")
    @ResponsePayload
    public UpdateMerchantResponse updateMerchant(@RequestPayload UpdateMerchantRequest request) {
        final UpdateMerchantResponse response = new UpdateMerchantResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();

        boolean isUpdated = merchantService.updateMerchant(request.getMerchant());

        if(isUpdated){
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Product updated successfully");
        } else {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while updating Product: " + request.getMerchant());
        }

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteMerchantRequest")
    @ResponsePayload
    public DeleteMerchantResponse deleteMerchant(@RequestPayload DeleteMerchantRequest request) {
        final DeleteMerchantResponse response = new DeleteMerchantResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();

        boolean isDeleted = merchantService.deleteMerchantById(request.getMerchantId());

        if(isDeleted){
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Product deleted successfully");
        } else {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while deleting product id: " + request.getMerchantId());
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addProductToMerchantRequest")
    @ResponsePayload
    public AddMerchantResponse addProductToMerchant(@RequestPayload AddProductToMerchantRequest request) {
        final AddMerchantResponse response = new AddMerchantResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();

        Merchant merchant = merchantService.addProductToMerchant(request.getMerchantId(), request.getProductId());

        if(merchant == null){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding a Merchant");
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Merchant Added Successfully");
        }

        response.setMerchant(merchant);
        response.setServiceStatus(serviceStatus);
        return response;
    }
}
