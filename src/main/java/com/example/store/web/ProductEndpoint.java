package com.example.store.web;

import com.example.store.model.*;
import com.example.store.services.IProductService;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class ProductEndpoint {

    private static final String NAMESPACE_URI = "http://mytest.com/gs-producing-web-service";

    private final IProductService productService;

    public ProductEndpoint(IProductService productService) {
        this.productService = productService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addProductRequest")
    @ResponsePayload
    public AddProductResponse addProduct(@RequestPayload AddProductRequest request){
        final AddProductResponse response = new AddProductResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();
        Product product = productService.addProduct(request);

        if(product == null){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding a product");
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setProduct(product);
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateProductRequest")
    @ResponsePayload
    public UpdateProductResponse updateProduct(@RequestPayload UpdateProductRequest request) {
        final UpdateProductResponse response = new UpdateProductResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();

        boolean isUpdated = productService.updateProduct(request.getProduct());

        if(isUpdated){
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Product updated successfully");
        } else {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while updating Product: " + request.getProduct());
        }

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteProductRequest")
    @ResponsePayload
    public DeleteProductResponse deleteProduct(@RequestPayload DeleteProductRequest request) {
        final DeleteProductResponse response = new DeleteProductResponse();
        final ServiceStatus serviceStatus = new ServiceStatus();

        boolean isDeleted = productService.deleteProductById(request.getProductId());

        if(isDeleted){
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Product deleted successfully");
        } else {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while deleting product id: " + request.getProductId());
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

}
